# Documentation en ligne 

##  Présentation NSI - Programmes - Progression 

* Présentation de la NSI Première   : https://info-am.irem.univ-mrs.fr/2019/static/docs/beffara.pdf

## Algorithmes

* Visualisation algorithme de tri : https://interstices.info/les-algorithmes-de-tri/
* Visualisation algorithme de tri : http://lwh.free.fr/pages/algo/tri/tri_insertion.html
* TP tri sur le site pixees.fr https://pixees.fr/informatiquelycee/n_site/nsi_prem_tri_algo.html
* TP k plus proches voisin : https://openclassrooms.com/fr/courses/4011851-initiez-vous-au-machine-learning/4022441-entrainez-votre-premier-k-nn
* Video P vs NP : https://www.youtube.com/watch?v=8TrIW-4kfRg


