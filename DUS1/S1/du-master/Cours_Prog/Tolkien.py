from dataclasses import dataclass


@dataclass
class Etre:

    appellation: str = ''
    poids      : int = 0
    bonjour    : str = ''

    def salut(self) -> None:
        print("%s ! Mon nom est %s" % (self.bonjour, self.appellation))

    def masse(self) -> None:
        print("Je pèse %d kg" % self.poids)


@dataclass
class Orque(Etre):
    
    bonjour: int = 'Ash nazg durbatulûk'
    maitre : str = 'Sauron'

    
class Hobbit(Etre):

    def __init__(self, nom: str, prenom: str, poids: int):
        Etre.__init__(self, prenom + ' ' + nom, poids, "Bonjour")

        
class Table:


    def __init__(self) -> None:
        self.idmax = 0 # identifiant maxi courant
        self.taille = 0 # taille du dictionnaire
        self.table = {} # le dictionnaire
        self.inscrits = self.table.values() # les inscrits
   
    def __repr__(self) -> str:
        t = self.table
        return str({k: str(t[k]) for k in t.keys()})

    def __getitem__(self, k) -> Inscrit:
        """ 
        Pour pouvoir utiliser les crochets
        Si T est un objet Table
        T[10] renvoie l'inscrit portant l'identifiant 10
        """
        return self.table[k]

#    def ajoute(self, p:Inscrit) -> None:


#    def enleve(self, p:Inscrit) -> None:
