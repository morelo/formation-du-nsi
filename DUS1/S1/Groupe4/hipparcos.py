"""
    Récupération et traitement de données astronomiques contenu dans un fichier
    Création d'une carte du ciel
"""

"""
    Liste des variables globales
"""
# Attributs (caractéritiques) des étoiles présent dans le fichier
tabAttributs=["HIP","Nc","RArad","DErad","P1","Hpmag","B-V","V-I"]
# dictionnaire des étoiles
dicoStars=[]
"""
   Récupére les données des étoiles dans une liste.
   Les données de chaque étoile sont classées dans un dictionnaire 
"""
def openStarsFile(namefile):
    """
    :param namefile: nom du fichier contenant les données des étoiles 1 ligne = 1 étoile
    :type namefile: str
    :return: retourne la liste des caractéristiques des étoiles
    :rtype: list
    """
    fichier = open(namefile, "r")
    dicoStars_=[]
    for line in fichier: # on lit ligne par ligne ne pas rajouter de fichier.readline() (lit une ligne sur 2)
        dico=createDicoStars_(line)
        dicoStars_.append(dico)
    fichier.close()
    return dicoStars_

"""
    affiche la valeur de la caractéristique attribut de chaque étoile
"""
def printOneAttributStars(attribut):
    """
    :param attribut: la caractéristique à afficher
    :type attribut: str
    :return: None
    """
    global dicoStars
    for i in range(len(dicoStars)):
        print("Star n°",int(dicoStars[i]["HIP"]),attribut,"=",dicoStars[i][attribut])

"""
    récupération des données d'une étoile dans le fichier 
    et création de son dictionnaire des caractéristiques
"""
def createDicoStars_(ligne):
    """
    :param ligne: ligne du fichier à traiter
    :type ligne: str
    :return: le dictionnaire des caractéristiques de l'étoile
    :rtype : dict
    """
    global tabAttributs
    ligne = ligne.replace("\n", "")# pour supprimer le retour à la ligne de la string
    lin=ligne.split(';') #transformation de la string en liste de strings en utilisant le séparateur ';'
    # float(lin [i]): on convertit chaque string en réel de la liste lin
    dico={tabAttributs[i]:float(lin [i]) for i in range(len(tabAttributs))} # construction du dictionnaire de l'étoile
    return dico
"""
    sélectionne les étoiles dont la caractéristique attribut vaut val
"""
def selectStars(attribut, val):
    """
    :param attribut: la caractéristique à étudier
    :param val: valeur de la caractéristique à tester
    :type attribut: str
    :type val: float
    :return: renvoie les noms des étoiles dont attribut vaut val
    :rtype : List
    """
    global dicoStars
    stars_=[]
    for i in range (len(dicoStars)):
        if (dicoStars[i][attribut]==val):
            stars_.append(int(dicoStars[i]["HIP"]))
    return stars_
    
    
def selectStars_encadrement(attribut, val1,val2):
    """
    :param attribut: la caractéristique à étudier
    :param val: valeur de la caractéristique à tester
    :type attribut: str
    :type val: float
    :return: renvoie les noms des étoiles dont attribut vaut val
    :rtype : List
    """
    global dicoStars
    stars_=[]
    for i in range (len(dicoStars)):
        if (dicoStars[i][attribut]<val2 and dicoStars[i][attribut]>val1):
            stars_.append(int(dicoStars[i]["HIP"]))
    return stars_

"""
    PROGRAMME PRINCIPAL
"""
dicoStars=openStarsFile("/home/morelo/Bureau/FormationNSI/S1/Groupe4/hip2_min.txt")
print(selectStars("Hpmag",8.5598))

printOneAttributStars("RArad")