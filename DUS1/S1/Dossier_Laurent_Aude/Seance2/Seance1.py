##
# Séance 1

#Projet de traîtement d'image en masse
#Extraire l'exif de photos en masse afin de générer un csv à destination opentreatmap
###

# -*- coding: utf-8 -*-

## gestion des lib


# Librairie os
import os

# Gestion des chemins relatifs
os.chdir ("/home/morelo/Documents/Depot_Formation_Du_NSI/DUS1/Dossier_Laurent_Aude/Seance2/")
print( "Chemin de travail : " ,os . getcwd ( ) )

# Déclaration des paths d'environnement  pour éventuellement l'importation d'une librairie

os.sys.path.append(os . getcwd ( ))



# Extraction du dictionnaire EXIF
import piexif # Gestion des dictionnaires EXIF 
from typing import List,  Dict , Tuple , Set   # Déclaration des Types
import folium   # Création d'une carte open streat map



## Travail sur le dictionnaire EXIF d'une image


# Fonction qui extra_exif
def extract_exif(file_name : str)->Dict:
    """
 
    Fonction qui retourne le dictionnaire EXIF brut
    >>> extract_exif("./20190425_100740.jpg")
    """
    exif_dict = piexif.load(file_name)
    return exif_dict


# Fonction de convertion GPS horaires en  GPS décimal
def conv_gps(Dic_GPS : Dict) ->Tuple:
    '''
    
    Fonction qui récupère dans le dictionnaire exif les coordonnées GPS en décimale
    >>>Dic_GPS = Dic["GPS"] = {0: (2, 2, 0, 0), 1: b'N', 2: ((47, 1), (37, 1), (185330, 10000))
, 3: b'W', 4: ((2, 1), (47, 1), (597175, 10000)), 5: 0, 6: (59, 1), 7: ((19, 1),
 (28, 1), (20, 1)), 27: b'ASCII\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
x00\x00\x00\x00\x00\x00\x00', 29: b'2019:04:24'}
    >>> conv_gps(Dic_GPS)
    '''
    #Latitude :
    if Dic_GPS[1]==b'N':
        Slat=1
    else :
        Slat=-1
    deg_lat, min_lat, sec_lat =float(Dic_GPS[2][0][0])/float(Dic_GPS[2][0][1])  ,  float(Dic_GPS[2][1][0])/float(Dic_GPS[2][1][1])  ,  float(Dic_GPS[2][2][0])/float(Dic_GPS[2][2][1])
    
    lat = Slat*(float(deg_lat) + float(min_lat)/60 + float(sec_lat)/(60*60))
    
    #Longitude :
    if Dic_GPS[3]==b'W':
        Slat=-1
    else :
        Slat=1
    deg_long, min_long, sec_long =float(Dic_GPS[4][0][0])/float(Dic_GPS[4][0][1]),float(Dic_GPS[4][1][0])/float(Dic_GPS[4][1][1]),float(Dic_GPS[4][2][0])/float(Dic_GPS[4][2][1])
    long = Slat*(float(deg_long) + float(min_long)/60 + float(sec_long)/(60*60))
    return (lat,long)

# Epuration du dictionnaire pour ne garder que les données utiles : marque, modèle, date , Coordonnées GPS

def epuration_EXIF (Dic_EXIF_BRUT : Dict) ->Dict :
    '''
    Fonction qui à un dictionnnaire EXIF brut associe un dictionnaire contenant : marque, modèle , date, lat , long 
    >>> epuration_EXIF ( extract_exif("./20190425_100740.jpg") )
    >>>{'marque': 'Sony', 'modele': 'G3312', 'date': '2019:04:28 21:09:07', 'lat': 48.673667888888886, 'long': -1.8590797222222224}
    '''

    Tuple_GPS =conv_gps(Dic_EXIF_BRUT["GPS"]) # Donne le tuple des coordonnées GPS décimal
    return  {"marque" : Dic_EXIF_BRUT["0th"][271].decode('utf-8').rstrip('\x00') , "modele" : Dic_EXIF_BRUT["0th"][272].decode('utf-8').rstrip('\x00') , "date" : Dic_EXIF_BRUT["0th"][306].decode('utf-8').rstrip('\x00') , "lat" : Tuple_GPS[0], 'long' :Tuple_GPS[1] }


# Génération de la carte : 
def gen_carte (Dico_epure: Dict) -> None :
    '''
    Fonction qui à un liste de dictionnaires d'images, crée une page html dans le répertoire courrant contenant la carte Openstreatmap des différentes positions GPS des images
    >>>gen_carte (Dico_epure)
    '''
    c= folium.Map(location=[Dico_epure["lat"],Dico_epure["long"]],zoom_start=1)        
    folium.Marker([Dico_epure["lat"],Dico_epure["long"]],popup=Dico_epure["date"]+" "+Dico_epure["marque"]).add_to(c)
    c.save('./maCarte.html')
    
    
    
## Programme 
if __name__ == "__main__":
    # exécuter la suite seulement sur le programme est lancer comme un script et pas comme une librairie
    path= input("donner le chemin relatif de l'image  ( exemple : ./images/mon_image.jpg)  : ")
    
    Dico=extract_exif(path)
    Dico_epure=epuration_EXIF(Dico)
    gen_carte (Dico_epure)
    print( "La carte a bien été réalisée ! ")
    
    

