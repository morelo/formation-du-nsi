###
#Projet de traîtement d'image en masse
#Extraire l'exif de photos en masse afin de générer un csv à destination opentreatmap
###

## gestion des lib


# Librairie os
import os
# Gestion des chemins relatifs
os.chdir ("/home/morelo/Documents/Depot_Formation_Du_NSI/DUS1/Dossier_Laurent_Aude/Seance2/")
print( "Chemin de travail : " ,os . getcwd ( ) )

# Déclaration des paths d'environnement  pour éventuellement l'importation d'une librairie
#path = "/home/morelo/Documents/Depot_Formation_Du_NSI/DUS1/Dossier_Laurent_Aude/"
os.sys.path.append(os . getcwd ( ))



# Extraction du dictionnaire EXIF
import piexif
import glob
from typing import List,  Dict , Tuple , Set
from Dicos import Dico_marques
import folium
import datetime



# Fonction qui extra_exif

def extract_exif(file_name :str) ->str :
    """
 
    Fonction qui retourne le dictionnaire EXIF brut
    >>> extract_exif("./20190425_100740.jpg")
    """
    exif_dict = piexif.load(abs_path(file_name))
    return exif_dict


# Fonction de convertion GPS horaires en  GPS décimal

def conv_gps(Dic_GPS :Dict) -> Tuple:
    '''
    
    Fonction qui récupère dans le dictionnaire exif les coordonnées GPS en décimale
    Dic["GPS"] = {0: (2, 2, 0, 0), 1: b'N', 2: ((47, 1), (37, 1), (185330, 10000))
, 3: b'W', 4: ((2, 1), (47, 1), (597175, 10000)), 5: 0, 6: (59, 1), 7: ((19, 1),
 (28, 1), (20, 1)), 27: b'ASCII\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
x00\x00\x00\x00\x00\x00\x00', 29: b'2019:04:24'}
    
    '''
    #Latitude :
    if Dic_GPS[1]==b'N':
        Slat=1
    else :
        Slat=-1
    deg_lat, min_lat, sec_lat =float(Dic_GPS[2][0][0])/float(Dic_GPS[2][0][1])  ,  float(Dic_GPS[2][1][0])/float(Dic_GPS[2][1][1])  ,  float(Dic_GPS[2][2][0])/float(Dic_GPS[2][2][1])
    print(deg_lat, min_lat, sec_lat)
    lat = Slat*(float(deg_lat) + float(min_lat)/60 + float(sec_lat)/(60*60))
    
    #Longitude :
    if Dic_GPS[3]==b'W':
        Slat=-1
    else :
        Slat=1
    deg_long, min_long, sec_long =float(Dic_GPS[4][0][0])/float(Dic_GPS[4][0][1]),float(Dic_GPS[4][1][0])/float(Dic_GPS[4][1][1]),float(Dic_GPS[4][2][0])/float(Dic_GPS[4][2][1])
    long = Slat*(float(deg_long) + float(min_long)/60 + float(sec_long)/(60*60))
    
    return (lat,long)




# Fonction Selection de dictionaire   select()
# Donne en sortie le dictionnaire correspondant aux critères
def select (Dic : Dict , Ens_keys  :Set) ->Set :
    """
    
    Crée un dictionnaire restraint à une liste de clés
    >>>Dic= {"fruit":3,"couleur" :5 , "voitures" : 6}
    >>>select(Dic ,{"fruit","couleur"})
    """
    return {key:Dic[key] for key in Dic.keys() if key in Ens_keys}
  
  
    
# Epuration du dictionnaire pour ne garder que les données utiles : marque, modèle, date , Coordonnées GPS
def epuration_EXIF (Dic_EXIF_BRUT : Dict ) -> Dict :
    '''
    
    Fonction qui à un dictionnaire Exif Brut , associe un dictionnaire contenant : Marque, Modèle, Date, Latitude, Longitude
    >>> epuration_EXIF ( extract_exif("./20190425_100740.jpg") )
    '''
    Dic_Oth_epure :Dict =select (Dic_EXIF_BRUT["0th"] , {271,272,306} )
    Tuple_GPS : Tuple  =conv_gps(select (Dic_EXIF_BRUT["GPS"] ,{1,2,3,4}) )  # Donne le tuple des coordonnées GPS décimal
    return  {"marque" : Dic_Oth_epure[271].decode().rstrip('\x00') , "modele" : Dic_Oth_epure[272].decode().rstrip('\x00') , "date" : Dic_Oth_epure[306].decode().rstrip('\x00') , "lat" : Tuple_GPS[0], 'long' :Tuple_GPS[1] }


# Fonction gestion de masse 
# Création d'une liste de  dictionnaires contenant le nom de l'image en clé et le dictionnaire associé en valeur. Toutes les images sont en jpg


def  traitement_dossier_dico(relpath_folder  :Str) -> Dict : #"./Images/#
    '''
    
    Fonction qui à un chemin de dossier d'image associe un dictionnaire de dictionnaire 
    '''
    image_files = glob.glob(relpath_folder+'*.JPG')
    print(image_files)
    Dic_Files={f[2:]:epuration_EXIF(piexif.load(f)) for f in image_files }  
    print(Dic_Files) 
    return Dic_Files



# Génération de la carte : 

def gen_carte (Dico_images : Dict)-> None :
    
    c= folium.Map(location=[47.078025, -2.409053],zoom_start=10)
    for key in Dico_images :
        print(key)
        
        folium.Marker([Dico_images[key]["lat"],Dico_images[key]["long"]],popup=key+" "+Dico_images[key]["date"]+" "+Dico_images[key]["marque"]).add_to(c)
    c.save('./maCarte'+str(datetime.datetime.now())+'.html')