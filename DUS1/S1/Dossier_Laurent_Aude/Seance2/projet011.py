###
#Projet de traîtement d'image en masse
#Extraire l'exif de photos en masse afin de générer un csv à destination opentreatmap
###

## gestion des lib


# Librairie os
import os
# Gestion des chemins relatifs
os.chdir ("/home/morelo/Documents/Depot_Formation_Du_NSI/DUS1/Dossier_Laurent_Aude/Seance2/")
print( "Chemin de travail : " ,os . getcwd ( ) )

# Déclaration des paths d'environnement  pour éventuellement l'importation d'une librairie

os.sys.path.append(os . getcwd ( ))

# Extraction du dictionnaire EXIF
import piexif # Gestion des dictionnaires EXIF 
import glob   # Recherche de chemins de style Unix selon certains motifs
from typing import List,  Dict , Tuple , Set   # Déclaration des Types
import folium   # Création d'une carte open streat map
import datetime # gestion du temps
from Seance1 import *


## Travail le le dictionnaire EXIF d'une image

## Traîtement en masse d'un dossier de photos
 
# Création d'une liste de  dictionnaires contenant le nom de l'image en clé et le dictionnaire associé en valeur. Toutes les images sont en jpg

def  traitement_dossier_dico(relpath_folder : str) ->List: #"./Images/#
    '''
    
    Fonction qui à un chemin de dossier d'image associe un dictionnaire de dictionnaire 
    >>> traitement_dossier_dico("./Images/")
    '''
    image_files = glob.glob(relpath_folder+'*.JPG')
    L_Dic=[]
    for f in image_files :
        Dic=epuration_EXIF(piexif.load(f))
        Dic["file"]=f[2:]
        L_Dic.append(Dic)
    return L_Dic 
    

# Selection de table 
#code écrit par CONNAN Guillaume
def select(table , critère) :

    def test(ligne) -> bool:
        return eval(critère)
    return [ligne for ligne in table if test(ligne)]


# Tri  de table 
#code écrit par CONNAN Guillaume
def tri(table, attribut, decroit:bool =False):
    """
    Tri selon un critère    
    """
    def critère(ligne) -> str:
        return ligne[attribut]
    return sorted(table, key=critère, reverse=decroit)



# Génération de la carte générale: 
def gen_carte (L_Dico : Dict) -> None :
    '''
    Fonction qui à un liste de dictionnaires d'images, crée une page html dans le répertoire courrant contenant la carte Openstreatmap des différentes positions GPS des images
    
    '''
    c= folium.Map(location=[47.078025, -2.409053],zoom_start=1)
    for Dico in L_Dico :
        
        folium.Marker([Dico["lat"],Dico["long"]],popup=Dico["file"]+" "+Dico["date"]+" "+Dico["marque"],icon=folium.Icon(color='green')).add_to(c)
        
    Ligne_Poly=[[Dico["lat"],Dico["long"]] for Dico in L_Dico ]   #Programme : Construction d'une liste par compréhension
    
    folium.PolyLine(Ligne_Poly, color="red", weight=2.5, opacity=1).add_to(c)
    c.save('./maCarte'+str(datetime.datetime.now())+'.html')
    ligne_poly=[]
 
 
# Génération d'une carte avec lignes par modèle

# Fonction ensemble de valeurs pour un attribut

def ensemble_valeurs_attribut (Table,attribut) :
    return {ligne[attribut] for ligne in Table}
    
# créations de la carte avec ligne plygonale par modèle

def carte_par_modele (Table):
    L_modeles=ensemble_valeurs_attribut (Table,"modele")
    print(L_modeles)
    c= folium.Map(location=[47.078025, -2.409053],zoom_start=1)
    for mod in L_modeles : 
        print("mod = " ,mod)
        #Création de la chaine de caractère Critère
        critere="ligne['modele']=="+"'"+mod+"'"
        print (critere)
        Table_modele= select(Table,critere)
        #Table_modele= tri(Table_modele,"date")        
        for Dico in Table_modele :            
            folium.Marker([Dico["lat"],Dico["long"]],popup=Dico["file"]+" "+Dico["date"]+" "+Dico["marque"]).add_to(c)            
        Ligne_Poly=[[Dico["lat"],Dico["long"]] for Dico in Table_modele]   #Programme : Construction d'une liste par compréhension
        folium.PolyLine(Ligne_Poly).add_to(c)
    c.save('./maCarte'+str(datetime.datetime.now())+'.html')
    Ligne_poly=[]
    
    
## Programme 
if __name__ == "__main__":
    # execute only if run as a script
    #Folder= str(input(" Donner le chemin du dossier à traîter  ex :   ./Images/   :  "))
    print( " Génération de la carte dans le dossier courrant...")
    gen_carte(tri(traitement_dossier_dico("./Images/"),"file"))
    print( " carte générée  : ", "maCarte",str(datetime.datetime.now()),".html")
    carte_par_modele (tri(traitement_dossier_dico("./Images/"),"file"))
    

