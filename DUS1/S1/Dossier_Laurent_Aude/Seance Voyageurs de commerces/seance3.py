###
#Projet de traîtement de données par table (liste de dictionnaire)
#réalsier des actions de sélection tri fusion sur des tables
###

# -*- coding: utf-8 -*-

## gestion des lib


# Librairie os
import os
# Gestion des chemins relatifs

os.chdir("/home/morelo/Documents/Depot_Formation_Du_NSI/DUS1/Dossier_Laurent_Aude/Seance3/")
print( "Chemin de travail : " ,os . getcwd ( ) )

# Déclaration des paths d'environnement  pour éventuellement l'importation d'une librairie

os.sys.path.append(os . getcwd ( ))

from typing import List,  Dict # Déclaration des Types
from copy import deepcopy # copie de ligne de tables
import csv  #lecture ecriture de fichier csv



Attribut = str
Valeur = str
Ligne = Dict[Attribut, Valeur]
Table = List[Ligne]

##lecture d'un fichier csv
 # variable qui permet d'ouvrir le fichier csv
 # __pédagogie__ mise en valeur de la commande open et de l'encodage commun

def depuis_csv(fichier_csv) -> list():
    """crée une liste de dictionnaire à partir d'un fichier csv
        la premiere ligne est celle des champs (clés)
    """
    lecteur=csv.DictReader(open(fichier_csv,'r'))
    return[dict(ligne) for ligne in lecteur]


## Affichage d'une citation
def affiche_cit(table_dec2):
    """affiche la citation la plus connue de Sherlock est élémenataire mon cher Watson, ainsi que celle des autres détectives du tableau 2
    """
    for i in range(len(table_dec2)):
        print("la citation la plus connue de",table_dec2[i]['nom'],"est :",table_dec2[i]['citation'])


## Selection dans une table
    #code écrit par CONNAN Guillaume
def select(table: Table, critère: str) -> Table:
    def test(ligne: Ligne) -> bool:
        return eval(critère)
    return [ligne for ligne in table if test(ligne)]

## Tri  de table
#code écrit par CONNAN Guillaume
def tri(table, attribut, decroit:bool =False):
    """
    Tri selon un critère
    """
    def critère(ligne) -> str:
        return ligne[attribut]
    return sorted(table, key=critère, reverse=decroit)

## Fusion de deux tables:
#code écrit par CONNAN Guillaume
def jointure(table1:Table, table2: Table, cle1:Attribut, cle2:Attribut =None)-> Table:
    if cle2 is None: # Par défaut les clés de jointure portent le même nom
        cle2 = cle1
    new_table = [] # La future table créée, vide au départ
    for ligne1 in table1:
        for ligne2 in table2:
# on ne considère que les lignes où les cellules de l'attribut choisi sont identiques.
            if ligne1[cle1] == ligne2[cle2]:
                new_line = deepcopy(ligne1) # on copie entièrement la ligne de table1
                for cle in ligne2: # on copie la ligne de table2 sans répéter la cellule de jointure
                    if cle != cle2:
                        new_line[cle] = ligne2[cle]
                    new_table.append(new_line)
    return new_table

## Ecriture d'un fichier csv
def vers_csv(nom_table,ordre_cols):  # mon_table est le nom de la table à convertir en csv au nom de mon_table.csv

        with open (nom_table + '.csv', 'w') as fic_csv:
            ecrit = csv.DictWriter(fic_csv,fieldnames=ordre_cols)
            table=eval(nom_table)
            ecrit.writeheader()
            for ligne in table:
                ecrit.writerow(ligne)
        return None


## Programme
if __name__ == "__main__":
    # exécuter la suite seulement sur le programme est lancer comme un script et pas comme une librairie
    #path= input("donner le chemin relatif de la table  ( exemple : ./seance3/table_dec1)  : ")

    #variables:
    #ouverture de fichier csv :
    fichier_Dec1_csv = open('./Detectives1_livre.csv','r',encoding="UTF-8")
    fichier_Dec2_csv = open('./Detectives2_livre.csv','r',encoding="UTF-8")
    
    #construction de table :
    #table_dec1=depuis_csv(fichier_Dec1_csv)
    #table_dec2 = depuis_csv(fichier_Dec2_csv)
    #table_dec3= jointure(table_dec1,table_dec1,'Nom','nom')







