
## gestion des lib


# Librairie os
import os
# Gestion des chemins relatifs
os.chdir ("/home/morelo/Documents/Depot_Formation_Du_NSI/DUS1/Dossier_Laurent_Aude/Seance3/")
print( "Chemin de travail : " ,os . getcwd ( ) )

# Déclaration des paths d'environnement  pour éventuellement l'importation d'une librairie

os.sys.path.append(os . getcwd ( ))


import glob   # Recherche de chemins de style Unix selon certains motifs
from typing import List,  Dict , Tuple , Set   # Déclaration des Types
import folium   # Création d'une carte open streat map
import datetime # gestion du temps
import csv
import math
from copy import deepcopy

# Gestion des fichiers csv 
# http://www.chicoree.fr/w/Fichiers_CSV_en_Python


def csv_vers_liste(path :str) -> List:
    L=[]
    with open(path, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            L.append(row)
    
    return [[ville[0],float(ville[1]),float(ville[2])] for ville in L]


#def table_vers_csv(Table :List, path :str )   #'names.csv' path : "./csv/export.csv"
#    with open(paths, 'w', newline='') as csvfile:
#        fieldnames = list(Table[0].keys())
#        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    
    #writer.writeheader()
     #   for Dico in Table :
            
            
    

def dist(ville1 :List, ville2 : List) -> float :
    # https://www.sunearthtools.com/fr/tools/distance.php    
    R = 6372.795477598  # rayon de la terre 
    return R*math.acos(math.sin(ville1[1]*math.pi/180)*math.sin(ville2[1]*math.pi/180)+math.cos(ville1[1]*math.pi/180)*math.cos(ville2[1]*math.pi/180)*math.cos((ville2[2]-ville1[2])*math.pi/180))
    


L_villes= csv_vers_liste("./ville_gps_9_villes.csv")   



# Calcul du trajet total d'une liste de villes
def distance_totale (L_villes : List) -> float :
    dist_tot=0
    for i in range (0,len(L_villes)-2):
    
        dist_tot=dist_tot+dist(L_villes[i],L_villes[i+1])
    dist_tot = dist_tot + dist(L_villes[0],L_villes[len(L_villes)-2])
    return dist_tot
    



print(distance_totale (L_villes))




# Python et les permutations :http://python.jpvweb.com/python/mesrecettespython/doku.php?id=permutations

def permutliste(seq, er=False, rep=False):
    """Retourne la liste de toutes les permutations de la liste seq (non récursif).
       si er=True: élimination des répétitions quand seq en a (ex: [1,2,2]
    """
    L_p = [seq]
    n = len(seq)
    for k in range(0,n-1):
        for i in range(0, len(L_p)):
            z = L_p[i][:]
            for c in range(0,n-k-1):
                z.append(z.pop(k))
                if er==False or (z not in p):
                    L_p.append(z[:])
    if rep ==False : 
        for p in L_p :
            p_tampon = deepcopy(p)
            if p_tampon in L_p :
                L_p.remove(p_tampon)
                
    return L_p




def liste_trajets(L_villes) :
    L_tampon= deepcopy(L_villes)
    Dep=L_tampon[0]
    L_tampon.remove(L_tampon[0])
    
    return [[Dep]+trajet for trajet in permutliste(L_tampon)]
    
 


def trajet_minimum (L_villes) :
    L_trajets = liste_trajets(L_villes)
    trajet_min = L_trajets[0]
    for trajet in L_trajets :
        if distance_totale (trajet)< distance_totale(trajet_min) :
            trajet_min=trajet
            print ( trajet_min , "     ", distance_totale(trajet_min))
    return trajet_min
    
            
            
            
# Génération de la carte : 
def gen_carte (trajet) -> None :
    '''
    Fonction qui à un liste de dictionnaires d'images, crée une page html dans le répertoire courrant contenant la carte Openstreatmap des différentes positions GPS des images
    
    '''
    c= folium.Map(location=[47.078025, -2.409053],zoom_start=4)
    for ville in trajet :
        
        folium.Marker([ville[1],ville[2]],popup=ville[0]).add_to(c)
        
    Ligne_Poly=[]
    
    for ville in trajet : 
        Ligne_Poly.append([ville[1],ville[2]])
    Ligne_Poly.append([trajet[0][1],trajet[0][2]])
        
    folium.PolyLine(Ligne_Poly).add_to(c)
    c.save('./maCarte'+str(datetime.datetime.now())+'.html')
    ligne_poly=[]


