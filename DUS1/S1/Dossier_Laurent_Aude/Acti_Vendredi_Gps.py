import piexif


#Extraction du dictionnaire
Dic_Exif=piexif.load("/home/morelo/Documents/Depot_Formation_Du_NSI/DUS1/Dossier_Laurent_Aude/Seance2/Images/20190425_100740.jpg")
print(Dic_Exif)

#Fonction de conversion des coordonnées GPS
def conv_gps(Dic):
    #Latitude :
    if Dic[1]==b'N':
        Slat=1
    else :
        Slat=-1
    deg_lat, min_lat, sec_lat =Dic[2][0][0],Dic[2][1][0],Dic[2][2][0]
    lat = Slat*(float(deg_lat) + float(min_lat)/60 + float(sec_lat)/(60*60))
    
    #Longitude :
    if Dic[3]==b'W':
        Slat=-1
    else :
        Slat=1
    deg_long, min_long, sec_long =Dic[4][0][0],Dic[4][1][0],Dic[4][2][0]
    long = Slat*(float(deg_long) + float(min_long)/60 + float(sec_long)/(60*60))
    
    return (lat,long)



#Construction de l'url Openstreatmap

Coord_Gps=conv_gps(Dic_Exif["GPS"])    
    
url = "https://www.openstreetmap.org/?mlat="+str(Coord_Gps[0])+"&mlon="+str(Coord_Gps[1])
print(url)