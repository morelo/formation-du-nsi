###
#Projet de traîtement d'image en masse
#Extraire l'exif de photos en masse afin de générer un csv à destination opentreatmap
###

## gestion des lib

import os
import piexif
import glob
from typing import List,  Dict


# Déclaration des paths d'environnement  pour éventuellement l'importation d'une librairie
path = "/home/morelo/Documents/Depot_Formation_Du_NSI/DUS1/Dossier_Laurent_Aude/"
os.sys.path.append(path)


# Gestion des chemins relatifs
os.chdir ("/home/morelo/Documents/Depot_Formation_Du_NSI/DUS1/Dossier_Laurent_Aude/")
print( "Chemin de travail : " ,os . getcwd ( ) )
    

# Fonction qui extra_exif

def extract_exif(file_name):
    """
    Fonction qui retourne le dictionnaire EXIF brut
    >>> extract_exif("./20190425_100740.jpg")
    """
    exif_dict = piexif.load(abs_path(file_name))
    return exif_dict


# Fonction de convertion GPS horaires en  GPS décimal

def conv_gps(Dic):
    '''
    
    Fonction qui récupère dans le dictionnaire exif les coordonnées GPS en décimale
    Dic["GPS"] = 'GPS': {0: (2, 2, 0, 0), 1: b'N', 2: ((47, 1), (37, 1), (185330, 10000))
, 3: b'W', 4: ((2, 1), (47, 1), (597175, 10000)), 5: 0, 6: (59, 1), 7: ((19, 1),
 (28, 1), (20, 1)), 27: b'ASCII\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\
x00\x00\x00\x00\x00\x00\x00', 29: b'2019:04:24'}
    
    '''
    #Latitude :
    if Dic[1]==b'N':
        Slat=1
    else :
        Slat=-1
    deg_lat, min_lat, sec_lat =Dic[2][0][0]/Dic[2][0][1]  ,  Dic[2][1][0]/Dic[2][1][0]  ,  Dic[2][2][0]/Dic[2][2][1]
    lat = Slat*(float(deg_lat) + float(min_lat)/60 + float(sec_lat)/(60*60))
    
    #Longitude :
    if Dic[3]==b'W':
        Slat=-1
    else :
        Slat=1
    deg_long, min_long, sec_long =Dic[4][0][0]/Dic[4][0][1],Dic[4][1][0]/Dic[4][1][1],Dic[4][2][0]/Dic[4][2][1]
    long = Slat*(float(deg_long) + float(min_long)/60 + float(sec_long)/(60*60))
    
    return (lat,long)




# Fonction Selection de dictionaire   select()
# Donne en sortie le dictionnaire correspondant aux critères
def select (Dic : Dict , Ens_keys ) :
    """
    
    Crée un dictionnaire restraint à une liste de clés
    >>>Dic= {"fruit":3,"couleur" :5 , "voitures" : 6}
    >>>select(Dic ,{"fruit","couleur"})
    """
    return {key:Dic[key] for key in Dic.keys() if key in Ens_keys}
    

    

# Fonction gestion de masse 
# Création d'une liste de  dictionnaires contenant le nom de l'image en clé et le dictionnaire associé en valeur. Toutes les images sont en jpg


def  traitement_dossier_dico(relpath_folder): #"./Dossier_Images/#
    image_files = glob.glob('./*.JPG')
    Dic_Files={f[2:]:piexif.load(f) for f in image_files }  
    print(Dic_Files) 


# Convertion du dictionnaire en fichier csv . Le fichier csv est créé dans le dossier des images

# https://deparkes.co.uk/2016/06/03/plot-lines-in-folium/




