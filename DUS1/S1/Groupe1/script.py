# -*- coding: utf-8 -*-
import glob
# The glob module finds all the pathnames matching a specified pattern according to the rules used by the Unix shell
from pathlib import Path
# The final path component, without its suffix
import csv
import folium




moncsv = "/home/morelo/Bureau/FormationNSI/S1/Groupe1/monuments_frequentation__0__2652.csv"
moncsvGPS = "/home/morelo/Bureau/FormationNSI/S1/Groupe1/monuments_GPS__0__2652.csv"






def depuis_csv(nom_fichier_csv: str):
    """
    Crée une liste de dictionnaires, un par ligne.
    La 1ère ligne du fichier csv est considérée comme la ligne des noms des champs
    """
    lecteur = csv.DictReader(open(nom_fichier_csv,'r')) 
    return [dict(ligne) for ligne in lecteur]


def select(table, critère):
    """
    
    Crée une table qui fait un filtre suivant unn critère sur une sélection
    """
    
    def test(ligne) :
        return eval(critère)
    return [ligne for ligne in table if test(ligne)]




def jointure(table1, table2 , cle1 , cle2: Attribut = None) :
    """
    Crée une table à partir de deux autres tables 
    
    """

    if cle2 is None: # Par défaut les clés de jointure portent le même nom
        cle2 = cle1
    new_table: Table = []  # La future table créée, vide au départ
    for ligne1 in table1: 
        for ligne2 in table2:
		    #  on ne  considère que  les lignes  où les  cellules de  l'attribut
		    # choisi sont identiques. 
            if ligne1[cle1] == ligne2[cle2]:
                new_line = deepcopy(ligne1) # on copie entièrement la ligne de table1 
                for cle in ligne2: # on copie la ligne de table2 sans répéter la
                                   # cellule de jointure
                    if cle != cle2:
                        new_line[cle] = ligne2[cle]
                new_table.append(new_line)
    return new_table




Dic= depuis_csv(moncsv)
Dic_Gps=depuis_csv(moncsvGPS)







Dic_select= select(Dic, "int(ligne['Frequentation'])>100000")


jointure(Dic,Dic_GPS,)

print ( Dic_select)





