# Préparation sur les algorithmes



## Algorithmes de tir



* Tri par sélection :  Recherche de la plus petite valeur dans la sélection restante - complexité en $$n^2$$
* Tri par insertion : insertion de la valeur en cours dans la liste précédente déjà triée - complexité en $$n^2 $$

